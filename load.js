

var loadState =
{
    preload:function()
    {
        var loadingLabel = game.add.text(game.width/2 , 150 ,
             'loading.....',{font:'30px Arial' , fill:'#ffffff'}); 
        loadingLabel.anchor.setTo(0.5 , 0.5);

        var progressBar = game.add.sprite(game.width/2 , 200 , 'progressBar');
        progressBar.anchor.setTo(0.5 , 0.5);
        game.load.setPreloadSprite(progressBar);
        game.load.image('pixel' , 'asset/blackdot.jpg');//add emitter picture
        game.load.image('background' , 'backGround.jpg');

        //game.load.baseURL = 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/';
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('dead' , 'manha.wav');
        game.load.audio('scream' , 'nail.wav');
        game.load.audio('duang' , 'dd.wav');
        game.load.audio('step' , 'step.wav');

        
    },
    create:function()
    {
        game.state.start('menu');
    }
};