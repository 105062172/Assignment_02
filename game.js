
/*var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');
game.state.add('boot' , bootState);
game.state.add('load' , loadState);
game.state.add('menu' , menuState);
game.state.add('play' , playState);


game.state.start('boot');
*/  


var player;
var keyboard;

var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;

var lastTime = 0;
var distance = 0;
var status = 'running';

var screamSound , deadSound , ddSound , stepSound;
var playState = 
{

    preload:function(){

/*game.load.baseURL = 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/';
game.load.crossOrigin = 'anonymous';
game.load.spritesheet('player', 'player.png', 32, 32);
game.load.image('wall', 'wall.png');
game.load.image('ceiling', 'ceiling.png');
game.load.image('normal', 'normal.png');
game.load.image('nails', 'nails.png');
game.load.spritesheet('conveyorRight', 'conveyor_right.png', 96, 16);
game.load.spritesheet('conveyorLeft', 'conveyor_left.png', 96, 16);
game.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
game.load.spritesheet('fake', 'fake.png', 96, 36);*/
    },


create:function()
{
this.emitter = game.add.emitter(0,0,15);
this.emitter.makeParticles('pixel');
this.emitter.setYSpeed(-150 , 150);
this.emitter.setXSpeed(-150 , 150);
this.emitter.setScale(2,0,2,0,800);
this.emitter.gravity = 0;
keyboard = game.input.keyboard.addKeys({
    'enter': Phaser.Keyboard.ENTER,
    'up': Phaser.Keyboard.UP,
    'down': Phaser.Keyboard.DOWN,
    'left': Phaser.Keyboard.LEFT,
    'right': Phaser.Keyboard.RIGHT,
    'w': Phaser.Keyboard.W,
    'a': Phaser.Keyboard.A,
    's': Phaser.Keyboard.S,
    'd': Phaser.Keyboard.D
});
game.add.image(0, 0 , 'background');
this.createBounders();
this.createPlayer();
this.createTextsBoard();
//add sound
screamSound = game.add.audio('scream');
deadSound = game.add.audio('dead');
ddSound = game.add.audio('duang');
stepSound = game.add.audio('step');
},

update:function () {

// bad
if(status == 'gameOver' && keyboard.enter.isDown) this.restart();
if(status != 'running') return;

this.physics.arcade.collide(player, platforms, this.effect);
this.physics.arcade.collide(player, [leftWall, rightWall]);
this.checkTouchCeiling(player);
this.checkGameOver();

this.updatePlayer();
this.updatePlatforms();
this.updateTextsBoard();

this.createPlatforms();
},

createBounders :function() {
leftWall = game.add.sprite(0, 0, 'wall');
game.physics.arcade.enable(leftWall);
leftWall.body.immovable = true;

rightWall = game.add.sprite(383, 0, 'wall');
game.physics.arcade.enable(rightWall);
rightWall.body.immovable = true;

ceiling = game.add.image(0, 0, 'ceiling');
},

//var lastTime = 0;
createPlatforms:function  () {
if(game.time.now > lastTime + 600) {
    lastTime = game.time.now;
    this.createOnePlatform();
    distance += 1;
}
},

createOnePlatform:function  () {

var platform;
var x = Math.random()*(400 - 96 - 40) + 20;
var y = 400;
var rand = Math.random() * 100;

if(rand < 20) {
    platform = game.add.sprite(x, y, 'normal');
} else if (rand < 40) {
    platform = game.add.sprite(x, y, 'nails');
    game.physics.arcade.enable(platform);
    platform.body.setSize(96, 15, 0, 15);
} else if (rand < 50) {
    platform = game.add.sprite(x, y, 'conveyorLeft');
    platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
    platform.play('scroll');
} else if (rand < 60) {
    platform = game.add.sprite(x, y, 'conveyorRight');
    platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
    platform.play('scroll');
} else if (rand < 80) {
    platform = game.add.sprite(x, y, 'trampoline');
    platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
    platform.frame = 3;
} else {
    platform = game.add.sprite(x, y, 'fake');
    platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
}

game.physics.arcade.enable(platform);
platform.body.immovable = true;
platforms.push(platform);

platform.body.checkCollision.down = false;
platform.body.checkCollision.left = false;
platform.body.checkCollision.right = false;
},

createPlayer: function()  {
player = game.add.sprite(200, 50, 'player');
player.direction = 10;
game.physics.arcade.enable(player);
player.body.gravity.y = 500;
player.animations.add('left', [0, 1, 2, 3], 8);
player.animations.add('right', [9, 10, 11, 12], 8);
player.animations.add('flyleft', [18, 19, 20, 21], 12);
player.animations.add('flyright', [27, 28, 29, 30], 12);
player.animations.add('fly', [36, 37, 38, 39], 12);
player.life = 10;
player.unbeatableTime = 0;
player.touchOn = undefined;
},

createTextsBoard :function  () {
var style = {fill: '#ff0000', fontSize: '20px'}
text1 = game.add.text(10, 10, '', style);
text2 = game.add.text(350, 10, '', style);
text3 = game.add.text(140, 200, 'Enter 重新開始', style);
text3.visible = false;
},
setPlayerAnimate:function (player) {
var x = player.body.velocity.x;
var y = player.body.velocity.y;

if (x < 0 && y > 0) {
    player.animations.play('flyleft');
}
if (x > 0 && y > 0) {
    player.animations.play('flyright');
}
if (x < 0 && y == 0) {
    player.animations.play('left');
}
if (x > 0 && y == 0) {
    player.animations.play('right');
}
if (x == 0 && y != 0) {
    player.animations.play('fly');
}
if (x == 0 && y == 0) {
  player.frame = 8;
}
},
updatePlayer:function  () {
if(keyboard.left.isDown) {
    player.body.velocity.x = -250;
} else if(keyboard.right.isDown) {
    player.body.velocity.x = 250;
} else {
    player.body.velocity.x = 0;
}
this.setPlayerAnimate(player);
},



updatePlatforms:function  () {
for(var i=0; i<platforms.length; i++) {
    var platform = platforms[i];
    platform.body.position.y -= 2;
    if(platform.body.position.y <= -20) {
        platform.destroy();
        platforms.splice(i, 1);
    }
}
},

updateTextsBoard:function  () {
if(player.life <= 0)
    text1.setText('life: 0');
else
    text1.setText('life:' + player.life);
text2.setText(distance);
},
conveyorRightEffect:function (player, platform) {
player.body.x += 2;
},

conveyorLeftEffect:function (player, platform) {
player.body.x -= 2;
},

trampolineEffect:function (player, platform) {
platform.animations.play('jump');
player.body.velocity.y = -350;
},

nailsEffect:function (player, platform) {
if (player.touchOn !== platform) {
    player.life -= 3;
    player.touchOn = platform;
    game.camera.flash(0xff0000, 100);
}
},

basicEffect:function (player, platform) {
if (player.touchOn !== platform) {
    if(player.life < 10) {
        player.life += 1;
    }
    player.touchOn = platform;
}
},

fakeEffect:function (player, platform) {
if(player.touchOn !== platform) {
    platform.animations.play('turn');
    setTimeout(function() {
        platform.body.checkCollision.up = false;
    }, 100);
    player.touchOn = platform;
}
},

effect:function (player, platform) {
if(platform.key == 'conveyorRight') {
    //this.conveyorRightEffect(player, platform);
    player.body.x += 2;
    stepSound.play();
}
if(platform.key == 'conveyorLeft') {
    //this.conveyorLeftEffect(player, platform);
    player.body.x -= 2;
    stepSound.play();
}
if(platform.key == 'trampoline') {
    //this.trampolineEffect(player, platform);
    platform.animations.play('jump');
    player.body.velocity.y = -350;
    ddSound.play();
}
if(platform.key == 'nails') {
    //this.nailsEffect(player, platform);
    if (player.touchOn !== platform) {
        player.life -= 3;
        player.touchOn = platform;
        screamSound.play();
        game.camera.flash(0xff0000, 100);
    }
}
if(platform.key == 'normal') {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        stepSound.play();
        player.touchOn = platform;
    }
    //this.basicEffect(player, platform);
}
if(platform.key == 'fake') {
    //this.fakeEffect(player, platform);
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}
},


checkTouchCeiling:function (player) {
if(player.body.y < 0) {
    if(player.body.velocity.y < 0) {
        player.body.velocity.y = 0;
    }
    if(game.time.now > player.unbeatableTime) {
        player.life -= 3;
        game.camera.flash(0xff0000, 100);
        player.unbeatableTime = game.time.now + 2000;
    }
}
},

checkGameOver:function  () {
if(player.life <= 0 || player.body.y > 500) {
    this.gameOver();
}
},

gameOver:function  () {
text3.visible = true;
platforms.forEach(function(s) {s.destroy()});
platforms = [];
status = 'gameOver';
this.emitter.x = player.x;
this.emitter.y = player.y;
deadSound.play();
this.emitter.start(true,800,null,15);



this.restart();
//game.state.start('boot');
},

restart:function  () {
text3.visible = false;
distance = 0;
this.createPlayer();
status = 'running';
game.state.start('boot');
}
};


/*var game = new Phaser.Game(600, 500, Phaser.AUTO, 'canvas');
game.state.add('boot' , bootState);
game.state.add('load' , loadState);
game.state.add('menu' , menuState);
game.state.add('play' , playState);


game.state.start('boot');*/
