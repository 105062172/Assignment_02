

var menuState = 
{
    create:function()
    {
        game.add.image(0, 0 , 'background');


        var nameLabel = game.add.text(game.width/2 , 80 , '小朋友下樓梯',{font:'30px Arial' , fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5 , 0.5);

        var startLabel = game.add.text(game.width/2 , game.height-80 , 'press ENTER to start game!',
                        {font:'30px Arial' , fill:'#ffffff'});
        startLabel.anchor.setTo(0.5 , 0.5);

        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start , this);
    },
    start:function()
    {
        game.state.start('play');
    },
};